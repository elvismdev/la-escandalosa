<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include_once(dirname(__FILE__).'/maofree_cashondeliveryfee.php');

$cashondelivery = new Maofree_CashOnDeliveryFee();

if ($cart->id_customer == 0 OR $cart->id_address_delivery == 0 OR $cart->id_address_invoice == 0 OR !$cashondelivery->active)
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

// Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
$authorized = false;
foreach (Module::getPaymentModules() as $module)
	if ($module['name'] == 'maofree_cashondeliveryfee')
	{
		$authorized = true;
		break;
	}
if (!$authorized)
	die(Tools::displayError('This payment method is not available.'));

$customer = new Customer((int)$cart->id_customer);

if (!Validate::isLoadedObject($customer))
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

/* Validate order */
if (Tools::getValue('confirm'))
{
	$total = $cart->getOrderTotal(true, Cart::BOTH);
	$cashondelivery->validateOrderCOD((int)($cart->id), Configuration::get('PS_OS_PREPARATION'), $total, $cashondelivery->displayName, NULL, array(), NULL, false,$customer->secure_key);
	$order = new Order((int)($cashondelivery->currentOrder));
	Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?key='.$customer->secure_key.'&id_cart='.(int)($cart->id).'&id_module='.(int)($cashondelivery->id).'&id_order='.(int)($cashondelivery->currentOrder));
}
else
{
	/* or ask for confirmation */
	$fee = (float)($cashondelivery->getCostCOD($cart, 2));
	$totalwithoutfee = (float)($cart->getOrderTotal(true, Cart::BOTH));
	$total = $fee + $totalwithoutfee;

	$smarty->assign(array(
		'COD_total' => $total,
		'COD_this_path' => __PS_BASE_URI__.'modules/maofree_cashondeliveryfee/',
		'COD_this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/maofree_cashondeliveryfee/'
	));

    $smarty->display('validation.tpl');
}

include(dirname(__FILE__).'/../../footer.php');
