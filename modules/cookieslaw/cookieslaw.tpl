{* Insert javascript customisation based on the options entered in the module config screen
*}

<!-- MODULE cookieslaw -->
<script type="text/javascript">
//<![CDATA[ 
{literal}
var user_options = {
{/literal}
"cookieTop":"{$cl_CookieTop}",
"messageContent1":"{l s='Esta web utiliza ' mod='cookieslaw'}",
"messageContent2":"{l s='cookies' mod='cookieslaw'}",
"messageContent3":"{l s='. Al utilizar nuestra web, aceptas el uso que hacemos.' mod='cookieslaw'}",
"messageContent4":"{l s='Si no haces nada, Asumimos que aceptas las condiciones.' mod='cookieslaw'}",
"cookieUrl":"{$cl_CookieUrl}",
"redirectLink":"{$cl_RedirectLink}",
"okText":"{l s='OK, acepto el uso de cookies' mod='cookieslaw'}",
"notOkText":"{l s='No Gracias' mod='cookieslaw'}",
"cookieName":"{$cl_CookieName}",
"cookiePath":"{$cl_CookiePath}",
"cookieDomain":"{$cl_CookieDomain}",
"ajaxUrl":"{$cl_ajaxUrl}"
{literal}
};
{/literal}
// ]]> 
</script>
<!-- /MODULE cookieslaw -->
