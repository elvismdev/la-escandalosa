<p>
	<img src="{$COD_this_path}img/cashondelivery.jpg" alt="{l s='Cash on delivery (COD) payment' mod='maofree_cashondeliveryfee'}" width="77" height="69" style="clear: both;" />
   <br /><br />{l s='Your order on' mod='maofree_cashondeliveryfee'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='maofree_cashondeliveryfee'}
	<br /><br />
	{l s='You have chosen the cash on delivery method.' mod='maofree_cashondeliveryfee'}
	<br /><br /><span class="bold">{l s='Your order will be sent very soon.' mod='maofree_cashondeliveryfee'}</span>
	<br /><br />{l s='For any questions or for further information, please contact our' mod='maofree_cashondeliveryfee'} <a href="{$link->getPageLink('contact-form.php', true)}">{l s='customer support' mod='maofree_cashondeliveryfee'}</a>.
</p>
