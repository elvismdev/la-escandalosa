{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block Viewed products --><div class="acc_head_3"><div class="cool">{l s='Lo más buscado...' mod='blockviewed_mod'}</div></div>
<div id="viewed-products_block_left" class="infiniteCarousel">
	<div class="flexslider carousel">
		<ul class="slides">
			{foreach from=$productsViewedObj item=viewedProduct name=myLoop}
				<li class="product_box_carousel">
				{$more_imgs = Product::getProductsImgs($viewedProduct->id)}
				{if count($more_imgs) > 0}
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');" style="margin-bottom: 10px; margin-top: 10px;">
						<div class="flipper">
							<div class="front_image">
								<a href="{$viewedProduct->product_link}" title="{l s='More about' mod='blockviewed_mod'} {$viewedProduct->name|escape:html:'UTF-8'}" class="content_img">
									<img src="{if isset($viewedProduct->id_image) && $viewedProduct->id_image}{$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, 'home_default')}{else}{$img_prod_dir}{$lang_iso}-default-medium_default.jpg{/if}"  />
								</a>
							</div>
								<div class="back_image">
									{foreach from=$more_imgs item=second_img}
										<a href="{$viewedProduct->product_link}" title="{l s='More about' mod='blockviewed_mod'} {$viewedProduct->name|escape:html:'UTF-8'}" class="content_img">
											<img src="{if isset($viewedProduct->id_image) && $viewedProduct->id_image}{$link->getImageLink($viewedProduct->link_rewrite, $second_img.id_image, 'home_default')}{else}{$img_prod_dir}{$lang_iso}-default-medium_default.jpg{/if}"  />
										</a>
									{/foreach}
								</div>
						</div>
					</div>
				{else}
					<div class="flip-container">
						<div>
							<div style="margin-left:2px; width:232px;">
								<a href="{$viewedProduct->product_link}" title="{l s='More about' mod='blockviewed_mod'} {$viewedProduct->name|escape:html:'UTF-8'}" class="content_img">
									<img src="{if isset($viewedProduct->id_image) && $viewedProduct->id_image}{$link->getImageLink($viewedProduct->link_rewrite, $viewedProduct->cover, 'home_default')}{else}{$img_prod_dir}{$lang_iso}-default-medium_default.jpg{/if}"  />
								</a>
							</div>
						</div>
					</div>
				{/if}
					<div class="product_title_carousel">
						<a href="{$viewedProduct->product_link}" title="{l s='More about' mod='blockviewed_mod'}"><span class="product_manufacturer">{$viewedProduct->manufacturer_name}</span>{if $viewedProduct->is_new == 1}<span class="product_new">{l s='Nuevo' mod='blockviewed_mod'}</span>{/if}{$viewedProduct->name}</a>
					</div>
					<div class="product_price_carousel">
						{if (!$PS_CATALOG_MODE AND ((isset($viewedProduct->show_price) && $viewedProduct->show_price) || (isset($viewedProduct->available_for_order) && $viewedProduct->available_for_order)))}
							{if isset($viewedProduct->show_price) && $viewedProduct->show_price && !isset($restricted_country_mode)}
								<span class="product_price" style="display: inline;">
									{if !$priceDisplay}
										{convertPrice price=$viewedProduct->price}
									{else}
										{convertPrice price=$viewedProduct->price_tax_exc}
									{/if}
								</span>
								{if isset($viewedProduct->reduction) && $viewedProduct->reduction}<span class="product_discount">{convertPrice price=convertPrice price=$viewedProduct->reduction + $viewedProduct->price}</span>{/if}
								<br />
							{/if}
							{if isset($viewedProduct->online_only) && $viewedProduct->online_only}
								<span class="online_only">
									{l s='Online only!'}
								</span>
							{/if}
							{if isset($viewedProduct->on_sale) && $viewedProduct->on_sale && isset($viewedProduct->show_price) && $viewedProduct->show_price && !$PS_CATALOG_MODE}
								<span class="product_on_sale">{l s='On sale!'}</span>
							{/if}
						{/if}
					</div>
				</li>
			{/foreach}
		</ul>
	</div>
</div>
