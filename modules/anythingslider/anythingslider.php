<?php

if (!defined('_PS_VERSION_'))

  exit;

	
class anythingslider extends Module
{
	public function __construct()
	{
		$this->name = 'anythingslider';
		$this->tab = 'front_office_features';
		$this->version = 1.0;
		$this->author = 'Polcoder';
		$this->need_instance = 0;

		parent::__construct();
		
		$this->displayName = $this->l('Polcoder AnythingSlider');
		$this->description = $this->l('HomePage slider with any kind of content - images & videos');
	}
	
	public function install()
	{
		if (parent::install() == false OR !$this->registerHook('displayHome'))
		return false;
		if (!Db::getInstance()->Execute('CREATE TABLE '._DB_PREFIX_.'anythingslider (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, `content` varchar(25555), `title` varchar(25555), `img` varchar(128), `url` varchar(128), `position` int(10), `active` int(10)) ENGINE=InnoDB default CHARSET=utf8'))
		return false;
		return true;
	}
	
	public function uninstall()
	{
	 	if (parent::uninstall() == false)
	 	return false;
		if (!Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'anythingslider`'))
	 	return false;
	 	return true;
	}
	
	public function getContent() 
	{	 
		$this->_generateForm();
		return $this->_html;
	}
	
	private function _generateForm() 
	{
		$presta = mysql_connect (_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die(mysql_error());
		$sel13 = mysql_select_db (_DB_NAME_,$presta);
		mysql_query("SET CHARSET 'utf8'");
		
		if(isset($_POST['saveit']))
		{
			if(isset($_POST['title_new'])AND($_POST['title_new']!=''))
			{
				if($_FILES["fileUpload"]["name"]!='')
				{
					mkdir('../img/'.time(), 0755);
					move_uploaded_file($_FILES["fileUpload"]["tmp_name"],'../img/'.time().'/'.$_FILES["fileUpload"]["name"]);
					$fileloc = '../img/'.time().'/'. $_FILES["fileUpload"]["name"];
				}
				else
				{
					$fileloc = '';
				}
				$content_n = $_POST['content_new'];
				$title_n = $_POST['title_new'];
				if(($content_n=='')and($fileloc!=''))
				{
					$image_ex = explode("img/",$fileloc);
					$content_n = '<img src="'._PS_BASE_URL_.__PS_BASE_URI__.'img/'.$image_ex[1].'" alt="'.$title_n.'">';
				}
				$url_n = $_POST['url_new'];
				$select_position = mysql_query("SELECT position FROM ps_anythingslider ORDER BY position DESC");
				if(mysql_num_rows($select_position)>0)
				{
					$result_position = mysql_fetch_array($select_position);
					$max_position = $result_position["position"];
					$new_position = $max_position+1;
				}
				else
				{
					$new_position = 0;
				}
				$insert = mysql_query("INSERT INTO ps_anythingslider VALUES ('','$content_n','$title_n','$fileloc','$url_n','$new_position','1')");
			}
			//update existing
			if(isset($_POST['id']))
			{
				$idi = $_POST['id'];
				$content = $_POST['content'];
				$title = $_POST['title'];
				$url = $_POST['url'];
				$position = $_POST['position'];
				
				for($i=0;$i<count($_POST['title']);$i++)
				{
					$content_u = $content[$i];
					$title_u = $title[$i];
					$url_u = $url[$i];
					$position_u = $position[$i];
					$idi_u = $idi[$i];
					
					$update = mysql_query("UPDATE ps_anythingslider SET content = '$content_u', title = '$title_u', url = '$url_u', position = '$position_u', active = '0' WHERE id = '$idi_u'");
					if(isset($_POST['del_t'][$i]))
					{
						$to_remove = $_POST['del_t'][$i];
						$remove = mysql_query("DELETE FROM ps_anythingslider WHERE id = '$to_remove'");
					}
				}
				if(isset($_POST['active_t']))
				{
					for($i=0;$i<count($_POST['active_t']);$i++)
					{
						$to_activate = $_POST['active_t'][$i];
						$update = mysql_query("UPDATE ps_anythingslider SET active = '1' WHERE id = '$to_activate'");
					}
				}
			}
		}
		$this->_html .= '<div style="width: 80%; border-top: 1px solid #e3e3e3; border-bottom: 2px solid #e3e3e3; margin: 0 auto; padding: 10px; background-color: #f4f4f4;">';
		$this->_html .= '<div style="display: block; width: 90%; padding: 10px; margin: 0 auto; font-size: 120%; border-bottom: 2px solid #38404b; margin-bottom:20px;">';
		$this->_html .= $this->l('Manage banners.');
		$this->_html .= '</div>';
		$this->_html .= '<form method="post" action="" enctype="multipart/form-data">';
		$select_slider = mysql_query("SELECT * FROM ps_anythingslider ORDER BY position ASC");
		if(mysql_num_rows($select_slider)>0)
		{
			while($result_slider = mysql_fetch_array($select_slider))
			{
				$id = $result_slider["id"];
				$img = $result_slider["img"];
				$content = $result_slider["content"];
				$title = $result_slider["title"];
				$url = $result_slider["url"];
				$position = $result_slider["position"];
				$active = $result_slider["active"];
				$this->_html .= '<div style="width:95%; margin:0 auto; display: block; border: 1px solid #d2d2d2; margin-bottom: 20px; padding-bottom:10px; background-color: #f9f9f9;">';
				$this->_html .= '<div style="width:250px; display:inline-block;vertical-align:top; margin:10px; overflow:hidden;"><img src="'.$img.'" height="80"></div>';
				$this->_html .= '<div style="width:400px; display:inline-block;vertical-align:top;">
					<div style="width:400px;"><div style="display:inline-block; width: 100px; text-align:right; margin-right: 10px; margin-top:10px;">Title</div><input type="text" name="title[]" style="width:250px;" value="'.$title.'"></div>
					<div style="width:400px;"><div style="display:inline-block; width: 100px; text-align:right; margin-right: 10px; margin-top:10px; vertical-align:top;">Content</div><textarea name="content[]" style="width:250px;margin-top: 10px;">'.$content.'</textarea></div>
					<div style="width:400px;"><div style="display:inline-block; width: 100px; text-align:right; margin-right: 10px; margin-top:10px;">URL</div><input type="text" name="url[]" style="width:250px;" value="'.$url.'"></div>
					</div>';
				if($active==1){$checked='checked';}else{$checked='';}
				$this->_html .= '<div style="width:600px; display:inline-block; vertical-align:top;">
					<div style="width:200px; display:inline-block;"><div style="display:inline-block; width: 100px; text-align:right; margin-right: 10px; margin-top:10px;">Position</div><input type="text" name="position[]" style="width:20px;" value="'.$position.'"></div>
					<div style="width:150px;display:inline-block;"><div style="display:inline-block; text-align:right; margin-right: 10px; margin-top:10px;">Active</div><input type="checkbox" name="active_t[]" value="'.$id.'" '.$checked.'></div>
					<div style="width:150px;display:inline-block;"><div style="display:inline-block; text-align:right; margin-right: 10px; margin-top:10px; color:red; font-weight: bold;">Delete</div><input type="checkbox" name="del_t[]" value="'.$id.'"></div>
					</div>';
				$this->_html .= '<input type="hidden" name="id[]" value="'.$id.'"></div>';
			}
		}
		$this->_html .= '<div style="width: 400px; display:block; margin-left: 40px; font-weight: bold; margin-bottom:10px;">'.$this->l('Add new banner.').'</div>';
		$this->_html .= '<div style="display:inline-block; width: 200px; text-align:right; margin-right: 10px;">Image</div><div style="display: inline-block;"><input type="file" name="fileUpload"id="fileUpload"></div><br>';
		$this->_html .= '<div style="display:inline-block; width: 200px; text-align:right; margin-right: 10px; margin-top:15px;">Title</div><div style="display: inline-block;"><input type="text" name="title_new" value="" style="width:438px;"></div><div style="display:inline-block; width: 200px; text-align:left; margin-left: 10px; font-size: 9px; color: red; margin-top:15px; vertical-align:top;">REQUIRED</div><br>';
		$this->_html .= '<div style="display:inline-block; width: 200px; text-align:right; margin-right: 10px; margin-top:15px; vertical-align:top;">Content<br><small>Video iframe</small></div><div style="display: inline-block;"><textarea name="content_new" style="width:438px; height:200px;margin-top: 15px;"></textarea></div><br>';
		$this->_html .= '<div style="display:inline-block; width: 200px; text-align:right; margin-right: 10px; margin-top:15px;">URL</div><div style="display: inline-block;"><input type="text" name="url_new" value="" style="width:438px;"></div><br>';
		$this->_html .= '<div style="display: block;">';
		$this->_html .= '<div style="display: inline-block;"><input style="padding: 5px;" type="submit" value="Save" name="saveit"/></div>';
		$this->_html .= '</div></div></form></div>';		
	}

	function hookhome()
	{
		$presta = mysql_connect (_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die(mysql_error());
		$sel13 = mysql_select_db (_DB_NAME_,$presta);
		mysql_query("SET CHARSET 'utf8'");
		global $smarty;
		
		$select = mysql_query("SELECT * FROM ps_anythingslider WHERE active = '1' ORDER BY position ASC");
		if(mysql_num_rows($select)>0)
		{
			while($result = mysql_fetch_array($select))
			{
				$img[] = $result["img"];
				$content[] = $result["content"];
				$title[] = $result["title"];
				$url[] = $result["url"];
			}
			
			$smarty->assign('img', $img);
			$smarty->assign('url', $url);
			$smarty->assign('content', $content);
			$smarty->assign('title', $title);
			
			Tools::addCSS(($this->_path).'anythingslider.css', 'all');
			Tools::addJS(($this->_path).'js/jquery.anythingslider.js');
			Tools::addJS(($this->_path).'js/jquery.anythingslider.video.js');
			Tools::addJS(($this->_path).'js/swfobject.js');
			return $this->display(__FILE__, 'anythingslider.tpl');
		}
	}
}
?>