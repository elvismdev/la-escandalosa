{capture name=path}{l s='Shipping' mod='maofree_cashondeliveryfee'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Order summation' mod='maofree_cashondeliveryfee'}</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<h3>Pagar en efectivo<!-- {l s='Cash on delivery (COD) payment' mod='maofree_cashondeliveryfee'} --></h3>
<br /><br />
<form action="{$COD_this_path_ssl}validation.php" method="post">
	<input type="hidden" name="confirm" value="1" />
	<p>
		<img src="{$COD_this_path}img/cashondelivery2.jpg" alt="{l s='Cash on delivery (COD) payment' mod='maofree_cashondeliveryfee'}" width="64" height="45" style="float:left; margin: 0px 10px 5px 0px;" />
		Ha elegido pagar en efectivo
		<!-- {l s='You have chosen the cash on delivery method.' mod='maofree_cashondeliveryfee'} -->
		<br/><br />
		{l s='The total amount of your order is' mod='maofree_cashondeliveryfee'}
		<span id="amount_{$currencies.0.id_currency}" class="price">{convertPrice price=$COD_total}</span> {if $use_taxes == 1}{l s='(tax incl.)' mod='maofree_cashondeliveryfee'}{/if}
	</p>
	<p>
		<br /><br />
		<br /><br />
		<b>Porfavor complete su orden haciendo click en "CONFIRMAR PEDIDO".
			<!-- {l s='Please confirm your order by clicking "I confirm my order"' mod='maofree_cashondeliveryfee'}. --></b>
	</p>
	<p class="cart_navigation">
		<a href="{$link->getPageLink('order.php', true)}?step=3" class="button_large hideOnSubmit">{l s='Other payment methods' mod='maofree_cashondeliveryfee'}</a>
		<input type="submit" name="submit" value="{l s='I confirm my order' mod='maofree_cashondeliveryfee'}" class="exclusive_large hideOnSubmit" />
	</p>
</form>