{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- MODULE Block new products -->
<div class="acc_head_3"><div class="cool">{l s='New products' mod='blocknewproducts_mod'}</div></div>
<div id="new-products_block_center" class="infiniteCarousel">
	<div class="flexslider carousel">
	{if $new_products !== false}
		<ul class="slides">
		{foreach from=$new_products item='product' name='newProducts'}
			<li class="product_box_carousel">
				{$more_imgs = Product::getProductsImgs($product.id_product)}
				{if count($more_imgs) > 0}
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');"style="margin-bottom: 10px;">
						<div class="flipper">
							<div class="front_image">
								<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
							</div>
								<div class="back_image">
									{foreach from=$more_imgs item=second_img}
										<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $second_img.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
									{/foreach}
								</div>
						</div>
					</div>
				{else}
					<div class="flip-container">
						<div>
							<div style="margin-left:2px; width:232px;">
								<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
							</div>
						</div>
					</div>
				{/if}
				<div class="product_title_carousel">
					<dt class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}"><a href="{$product.link}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}"><span class="product_manufacturer">{$product.manufacturer_name}</span><span class="product_new">{l s='Nuevo' mod='blocknewproducts_mod'}</span>{$product.name|strip_tags|escape:html:'UTF-8'}</a></dt>
				</div>
				<div class="product_price_carousel">
						{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
							{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
								<span class="product_price" style="display: inline;">
									{if !$priceDisplay}
										{convertPrice price=$product.price}
									{else}
										{convertPrice price=$product.price_tax_exc}
									{/if}
								</span>
								{if isset($product.reduction) && $product.reduction}
									<span class="product_discount">{convertPrice price=$product.price_without_reduction}</span>
								{/if}
								<br />
							{/if}
							{if isset($product.online_only) && $product.online_only}
								<span class="online_only">{l s='Online only!'}</span>
							{/if}
						{/if}
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
							<span class="product_on_sale">{l s='On sale!'}</span>
						{/if}
				</div>
				{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
					{if ($product.quantity > 0 OR $product.allow_oosp)}
						<div class="hidden_cart"><a class="exclusive ajax_add_to_cart_button" rel="ajax_id_product_{$product.id_product}" href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='blocknewproducts_mod'}">{l s='Añadir al carrito' mod='blocknewproducts_mod'}</a></div>
					{else}
						<div class="hidden_cart"><a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}"><span class="exclusive">{l s='View' mod='blocknewproducts_mod'}</span></a></div>
					{/if}
				{else}
					<div style="height:23px;"></div>
				{/if}
			</li>
		{/foreach}
		</ul>
	{else}
		<p>&raquo; {l s='No new products at this time' mod='blocknewproducts_mod'}</p>
	{/if}
	</div>
</div>
<!-- /MODULE Block new products -->