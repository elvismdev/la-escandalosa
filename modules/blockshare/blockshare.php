<?php

class BlockShare extends Module
{
	function __construct()
	{
		$this->name = 'blockshare';
		if(_PS_VERSION_ > "1.5.0.0"){
		$this->tab = 'social_networks';
		$this->author = 'RSI';
		$this->need_instance = 0;
		}
		elseif
		(_PS_VERSION_ > "1.4.0.0" && _PS_VERSION_ < "1.5.0.0"){
				$this->tab = 'front_office_features';
		$this->author = 'RSI';
		$this->need_instance = 0;
		}
		else
		{
		$this->tab = 'Blocks';
		}
		$this->version = 1.0;

		parent::__construct();

		$this->displayName = $this->l('Block share');
		$this->description = $this->l('Adds a block to display a share button-www.catalogo-onlinersi.com.ar');
	}

	function install()
	{
		if (!parent::install())
			return false;
		if (!$this->registerHook('productActions'))
			return false;
		return true;
	}

	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/
	function hookproductActions($params)
	{
		global $smarty, $protocol_content, $server_host;

		
		return $this->display(__FILE__, 'blockshare.tpl');
	}

}

?>