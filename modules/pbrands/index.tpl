<link rel="stylesheet" href="pbrands.css" type="text/css">
<script type="text/javascript">

$(document).ready(function() {

	$(".pbrands_container").hide();
	$(".pbrands_container:first").show(); 

	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active");
		$(this).addClass("active");
		$(".pbrands_container").hide();
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab).fadeIn(); 
	});
});

</script> 
<div class="pbrands_header">{l s='Brands' mod='pbrands'}</div>
<div class="pbrands_header_promo">{l s='Premium' mod='pbrands'}</div>
<div class="pbrands_letters" id="slider-id">
	<ul class="tabs">
		<li class="active" rel="all">{l s='Tutti' mod='pbrands'}</li>
		{section name=letters loop=$vars start=1}
			<li rel="{$vars[letters].letter}">{$vars[letters].letter}</li>
		{/section}
	</ul>
</div>
	<div class="pbrands_left">
		<div class="tab_container">
			<div class="pbrands_container" id="all">
				{section name=letters loop=$vars start=1}
				<span class="pbrands_letter">{$vars[letters].letter}</span><div class="fill"></div>
				<div class="spinacz">
					<div class="pbrands_container_all">
					{section name=manufacturer_name loop=$manufacturers_{$vars[letters].letter}}
						<div class="pbrands_single">
							{$manufacturers_{$vars[letters].letter}[manufacturer_name]}
						</div>
					{/section}
					</div>
					<div class="pbrands_container_all_promo">
					{section name=pmanufacturer_name loop=$pmanufacturers_{$vars[letters].letter}}
						<div class="pbrands_single_promo">
							{$pmanufacturers_{$vars[letters].letter}[pmanufacturer_name]}
						</div>
					{/section}
					</div>
				</div>
				{/section}			
			</div>
			{section name=brands loop=$vars}
			<div class="pbrands_container" id="{$vars[brands].letter}">
				<span class="pbrands_letter">{$vars[brands].letter}</span><div class="fill"></div>
				<div class="spinacz">
					<div class="pbrands_container_all">
					{section name=manufacturer_name loop=$manufacturers_{$vars[brands].letter}}
						<div class="pbrands_single">
							{$manufacturers_{$vars[brands].letter}[manufacturer_name]}
						</div>
					{/section}
					</div>
					<div class="pbrands_container_all_promo">
					{section name=pmanufacturer_name loop=$pmanufacturers_{$vars[brands].letter}}
						<div class="pbrands_single_promo">
							{$pmanufacturers_{$vars[brands].letter}[pmanufacturer_name]}
						</div>
					{/section}
					</div>
				</div>
			</div>
			{/section}
		</div>
	</div>