<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{uecookie}prestashop>uecookie_77dd54e4f719c6fb0c2e23452eb830da'] = 'Unión Europea \"Cookie\" ley';
$_MODULE['<{uecookie}prestashop>uecookie_fba6a93b46d375a50cb1c58b84899053'] = 'Este módulo muestra una buena información sobre las Cookies en su tienda';
$_MODULE['<{uecookie}prestashop>uecookie_1061fda795260b08e769c493105557f7'] = 'Esta tienda utiliza cookies y otras tecnologías para que podamos mejorar su experiencia en nuestros sitios.';
$_MODULE['<{uecookie}prestashop>uecookie_d12652129be13b431f0e491afac0d4fe'] = 'Unión Europea \"Cookie\" ley';
$_MODULE['<{uecookie}prestashop>uecookie_4d1d06e0e2f0a31f2806796bf0513c1a'] = 'texto aquí';
$_MODULE['<{uecookie}prestashop>uecookie_43781db5c40ecc39fd718685594f0956'] = 'Salvar';
