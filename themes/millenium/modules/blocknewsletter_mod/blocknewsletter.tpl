{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block Newsletter module-->

<div class="block_footer_generic">
	<h4>{l s='Newsletter' mod='blocknewsletter_mod'}</h4>
	<div class="block_footer_list">
	{if isset($msg) && $msg}
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg}</p>
	{/if}
		<form action="{$link->getPageLink('index')}" method="post">
				{* @todo use jquery (focusin, focusout) instead of onblur and onfocus *}
				<input class="newsletter_block_content_input" type="text" name="email" value="{if isset($value) && $value}{$value}{else}{l s='tu e-mail' mod='blocknewsletter_mod'}{/if}" 
					onfocus="javascript:if(this.value=='{l s='tu e-mail' mod='blocknewsletter_mod'}')this.value='';" 
					onblur="javascript:if(this.value=='')this.value='{l s='tu e-mail' mod='blocknewsletter_mod'}';" />
				<!--<select name="action">
					<option value="0"{if isset($action) && $action == 0} selected="selected"{/if}>{l s='Subscribe' mod='blocknewsletter_mod'}</option>
					<option value="1"{if isset($action) && $action == 1} selected="selected"{/if}>{l s='Unsubscribe' mod='blocknewsletter_mod'}</option>
				</select>-->
					<input type="submit" value="{l s='Enviar' mod='blocknewsletter_mod'}" class="newsletter_block_submit" name="submitNewsletter" />
				<input type="hidden" name="action" value="0" />
		</form>
	</div>
    <div class="block_footer_list">
    <h4>{l s='Siguenos' mod='blocksocial_mod'}</h4>
 	<ul class="tt-wrapper">
    	{if $facebook_url != ''}<li><a href="{$facebook_url|escape:html:'UTF-8'}"><img src="{$img_dir}facebook.png" border=0><span>Facebook</span></a></li>{/if}
		{if $twitter_url != ''}<li><a href="{$twitter_url|escape:html:'UTF-8'}"><img src="{$img_dir}twitter.png" border=0><span>Twitter</span></a></li>{/if}
		{if $google_plus != ''}<li><a href="{$google_plus|escape:html:'UTF-8'}"><img src="{$img_dir}google_plus.png" border=0><span>Google Plus</span></a></li>{/if}
		{if $you_tube != ''}<li><a href="{$you_tube|escape:html:'UTF-8'}"><img src="{$img_dir}you_tube.png" border=0><span>You Tube</span></a></li>{/if}
		{if $pinterest != ''}<li><a href="{$pinterest|escape:html:'UTF-8'}"><img src="{$img_dir}pinterest.png" border=0><span>Pinterest</span></a></li>{/if}
		{if $rss_url != ''}<li><a href="{$rss_url|escape:html:'UTF-8'}"><img src="{$img_dir}rss.png" border=0><span>Rss</span></a></li>{/if}
    </ul>
    </div>
 
</div>

<!-- /Block Newsletter module-->
