<?php
if (!defined('_PS_VERSION_'))
  exit;
 
class pbrands extends Module
{
	public function __construct()
	{
		$this->name = 'pbrands';
		$this->tab = 'Test';
		$this->version = 1.0;
		$this->author = 'Polcoder';
		$this->need_instance = 0;
	 
		parent::__construct();
	 
		$this->displayName = $this->l('Polcoder Manufacturers List');
		$this->description = $this->l('Display list of manufacturers in the storefront');
	}
	 

	public function install()
	{
		if (parent::install() == false OR !$this->registerHook('leftColumn'))
		return false;
		if (!Db::getInstance()->Execute('CREATE TABLE '._DB_PREFIX_.'polbrands (`id_brand` int(10)) ENGINE=InnoDB default CHARSET=utf8'))
		return false;
		return true;
	}
	
	public function uninstall()
	{
	 	if (parent::uninstall() == false)
	 		return false;
		if (!Db::getInstance()->Execute('DROP TABLE '._DB_PREFIX_.'polbrands'))
	 		return false;
	 	return true;
	}
	
	public function getContent() 
	{
 
		if(Tools::isSubmit('submit_text')) {
	  
		  Configuration::updateValue(
				  $this->name.'_text_to_show',
				  Tools::getValue('the_text')
				  );
		}
	 
		$this->_generateForm();
		return $this->_html;
	}
 
	private function _generateForm() 
	{
		$presta = mysql_connect (_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die(mysql_error());
		$sel13 = mysql_select_db (_DB_NAME_,$presta);
		mysql_query("SET CHARSET 'utf8'");
		
		if (isset($_POST['fakefield'])) 
		{
			$brand = $_POST['brand'];
			if(!empty($brand))
			{
				$clear_brands = mysql_query("DELETE FROM ps_polbrands");
				$N = count($brand);
				for($i=0; $i < $N; $i++)
				{
				  $brand_id = $brand[$i];
				  $put_brand = mysql_query("INSERT INTO ps_polbrands VALUES ($brand_id)");
				}
			}
		}
		$this->_html .= '<div style="width: 80%; border-top: 1px solid #e3e3e3; border-bottom: 2px solid #e3e3e3; margin: 0 auto; padding: 10px; background-color: #f4f4f4;">';
		$this->_html .= '<form method="post" action="">';
		$st = 1;
		$this->_html .= '<div style="display: block; width: 90%; padding: 10px; margin: 0 auto; font-size: 120%; border-bottom: 2px solid #38404b;">';
		$this->_html .= $this->l('Select brands which you want to display in a Premium Brands section.');
		$this->_html .= '</div>';
		$get_manufacturers = mysql_query("SELECT * FROM ps_manufacturer ORDER BY name ASC");
		$prev = '';
		WHILE($manufacturer_data = mysql_fetch_array($get_manufacturers))
		{
			$act = substr($manufacturer_data["name"], 0, 1 );
			if($st==1)
			{
				if(is_numeric($act))
				{$this->_html .= '<div style="display: block; margin-top: 20px; text-transform: uppercase; font-size: 250%; width: 99%; background-color: #eeeeee; color: #6a6a6a; padding-left: 10px; margin-bottom: 5px;">0-9</div>';}
				if(($act!=$prev)AND(!is_numeric($act)))
				{$this->_html .= '<div style="display: block; margin-top: 20px; text-transform: uppercase; font-size: 250%; width: 99%; background-color: #eeeeee; color: #6a6a6a; padding-left: 10px; margin-bottom: 5px;">'.$act.'</div>';
					$st = 1;}
				else{$st++;}
				$st++;
			}
			else
			{
				if(($act!=$prev)AND(!is_numeric($act)))
				{$this->_html .= '<div style="display: block; margin-top: 20px; text-transform: uppercase; font-size: 250%; width: 99%; background-color: #eeeeee; color: #6a6a6a; padding-left: 10px; margin-bottom: 5px;">'.$act.'</div>';
					$st = 1;}
				else{$st++;}
			}
			$manu_id = $manufacturer_data["id_manufacturer"];
			$get_checked = mysql_query("SELECT * FROM ps_polbrands WHERE id_brand = '$manu_id'");
			if(mysql_num_rows($get_checked)>0){$chk = 'checked';}else{$chk = '';}
			$this->_html .= '<div style="width: 20%; display: inline-block; margin-bottom: 4px;"><input type="checkbox" name="brand[]" value="'.$manu_id.'" '.$chk.'> ' . $manufacturer_data["name"] . '</div>';
			$prev = substr($manufacturer_data["name"], 0, 1 );	
		}
		$this->_html .= '<input type="text" name="fakefield" id="fakefield" hidden>';
		$this->_html .= '<div style="display: block;"><input style="padding: 5px;" name="submit" type="submit" value="Save" name="save"/><div>';
		$this->_html .= '</div></form>';
	}
}
?>