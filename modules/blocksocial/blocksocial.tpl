{*
* 2007-2012 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="social_block">
	<div class="social_block_header">{l s='Follow us' mod='blocksocial'}</div>
	<div class="social_block_content"><ul class="tt-wrapper">
		{if $facebook_url != ''}<li><a href="{$facebook_url|escape:html:'UTF-8'}"><img src="{$base_dir}img/facebook.png" border=0><span>Facebook</span></a></li>{/if}
		{if $twitter_url != ''}<li><a href="{$twitter_url|escape:html:'UTF-8'}"><img src="{$base_dir}img/twitter.png" border=0><span>Twitter</span></a></li>{/if}				{if $google_plus != ''}<li><a href="{$google_plus|escape:html:'UTF-8'}"><img src="{$base_dir}img/google_plus.png" border=0><span>Google Plus</span></a></li>{/if}				{if $you_tube != ''}<li><a href="{$you_tube|escape:html:'UTF-8'}"><img src="{$base_dir}img/you_tube.png" border=0><span>You Tube</span></a></li>{/if}				{if $pinterest != ''}<li><a href="{$pinterest|escape:html:'UTF-8'}"><img src="{$base_dir}img/pinterest.png" border=0><span>Pinterest</span></a></li>{/if}
		{if $rss_url != ''}<li><a href="{$rss_url|escape:html:'UTF-8'}"><img src="{$base_dir}img/rss.png" border=0><span>Rss</span></a></li>{/if}
	</div></ul>
</div>
