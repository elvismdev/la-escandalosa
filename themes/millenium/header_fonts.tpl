{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
	<head>
    <link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:700,400' rel='stylesheet' type='text/css'>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<meta http-equiv="content-language" content="{$meta_language}" />
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<script type="text/javascript">
			var baseDir = '{$content_dir}';
			var baseUri = '{$base_uri}';
			var static_token = '{$static_token}';
			var token = '{$token}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
		</script>
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
	<link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
	{/foreach}
{/if}
{if isset($js_files)}
	{foreach from=$js_files item=js_uri}
	
	<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
{/if}
		{$HOOK_HEADER}
	
<link rel="stylesheet" href="{$base_dir}js/css/ezmark.css" media="all">
<script type="text/javascript" language="Javascript" src="{$base_dir}js/jquery.ezmark.min.js"></script>
<script type="text/javascript">

$.fn.infiniteCarousel = function () {

    function repeat(str, num) {
        return new Array( num + 1 ).join( str );
    }
  
    return this.each(function () {
        var $wrapper = $('> div', this).css('overflow', 'hidden'),
            $slider = $wrapper.find('> ul'),
            $items = $slider.find('> li'),
            $single = $items.filter(':first'),
            
            singleWidth = $single.outerWidth(), 
            visible = 4, // note: doesn't include padding or border
            currentPage = 1,
            pages = Math.ceil($items.length / visible);            


        // 1. Pad so that 'visible' number will always be seen, otherwise create empty items
        if (($items.length % visible) != 0) {
            $slider.append(repeat('<li class="empty" />', visible - ($items.length % visible)));
            $items = $slider.find('> li');
        }

        // 2. Top and tail the list with 'visible' number of items, top has the last section, and tail has the first
        $items.filter(':first').before($items.slice(- visible).clone().addClass('cloned'));
        $items.filter(':last').after($items.slice(0, visible).clone().addClass('cloned'));
        $items = $slider.find('> li'); // reselect
        
        // 3. Set the left position to the first 'real' item
        $wrapper.scrollLeft(singleWidth * visible);
        
        // 4. paging function
        function gotoPage(page) {
            var dir = page < currentPage ? -1 : 1,
                n = Math.abs(currentPage - page),
                left = singleWidth * dir * visible * n;
            
            $wrapper.filter(':not(:animated)').animate({
                scrollLeft : '+=' + left
            }, 900, function () {
                if (page == 0) {
                    $wrapper.scrollLeft(singleWidth * visible * pages);
                    page = pages;
                } else if (page > pages) {
                    $wrapper.scrollLeft(singleWidth * visible);
                    // reset back to start position
                    page = 1;
                } 

                currentPage = page;
            });                
            
            return false;
        }
        
        $wrapper.after('<a class="arrow back">&lt;</a><a class="arrow forward"></a>');
        
        // 5. Bind to the forward and back buttons
        $('a.back', this).click(function () {
            return gotoPage(currentPage - 1);                
        });
        
        $('a.forward', this).click(function () {
            return gotoPage(currentPage + 1);
        });
        
        // create a public interface to move to a specific page
        $(this).bind('goto', function (event, page) {
            gotoPage(page);
        });
    });  
};

$(document).ready(function () {
  $('.infiniteCarousel').infiniteCarousel();
});
</script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Armata|Duru+Sans|Ropa+Sans|Arimo|Monda|Marcellus|Oxygen|Scada|Alegreya+SC|Enriqueta|Signika' rel='stylesheet' type='text/css'>
	</head>
	
	<body {if isset($page_name)}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if} class="{if $hide_left_column}hide-left-column{/if} {if $hide_right_column}hide-right-column{/if}">
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<div id="page" class="container_9 clearfix">
			<div class="header_background"></div>
			<!-- Header -->
			<div id="header" class="grid_9 alpha omega">
				<div id="header_logo">
					<a href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
						<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if} />
					</a>
				</div>
				<div id="header_right" class="grid_9 alpha omega">
					{$HOOK_TOP}
					
					{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}{include file="$tpl_dir./breadcrumb.tpl"}{/if}
					
					{if isset($category) && $page_name != 'product'}
						{if $category->id AND $category->active}
							<div class="PS_category_header">
								<h1>
									{strip}
										{$category->name|escape:'htmlall':'UTF-8'}
										{if isset($categoryNameComplement)}
											{$categoryNameComplement|escape:'htmlall':'UTF-8'}
										{/if}
									{/strip}
								</h1>
							<div>
						{/if}
					{/if}
					{if isset($manufacturer->id) && $page_name == 'manufacturer'}
							<div class="PS_category_header">
								<h1>
									{$manufacturer->name|escape:'htmlall':'UTF-8'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'new-products'}
							<div class="PS_category_header">
								<h1>
									{capture name=path}{l s='New products'}{/capture}
									{l s='New products'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'best-sales'}
							<div class="PS_category_header">
								<h1>
									{capture name=path}{l s='Top sellers'}{/capture}
									{l s='Top sellers'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'manufacturer' && !isset($manufacturer->id)}
							<div class="PS_category_header">
								<h1>
									{capture name=path}{l s='Manufacturers'}{/capture}
									{l s='Manufacturers'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'supplier'}
							<div class="PS_category_header">
								<h1>
									{capture name=path}{l s='Suppliers'}{/capture}
									{l s='Supplier: '}{$supplier->name|escape:'htmlall':'UTF-8'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'prices-drop'}
							<div class="PS_category_header">
								<h1>
									{capture name=path}{l s='Price drop'}{/capture}
									{l s='Price drop'}
								</h1>
							<div>
					{/if}
					{if $page_name == 'search'}
						<div class="PS_category_header">
							<h1>
								{l s='Search'}&nbsp;{if $nbProducts > 0}"{if isset($search_query) && $search_query}{$search_query|escape:'htmlall':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'htmlall':'UTF-8'}{elseif $ref}{$ref|escape:'htmlall':'UTF-8'}{/if}"{/if}
{if isset($instantSearch) && $instantSearch}<a href="#" class="close">{l s='Return to previous page'}</a>{/if}
							</h1>
						<div>
					{/if}
				</div>
			</div>

			<div id="columns" class="grid_9 alpha omega clearfix">
				<!-- Left -->
				{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}
				<div id="left_column" class="column grid_2 alpha">
					{$HOOK_LEFT_COLUMN}
				</div>
				{/if}

				<!-- Center -->
				{if $page_name != 'category' && $page_name != 'manufacturer' && $page_name != 'search' && $page_name != 'new-products' && $page_name != 'best-sales' && $page_name != 'prices-drop' && $page_name != 'supplier'}<div id="center_column" class="grid_9 alpha omega clearfix">{/if}
				{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}<div id="center_column" class=" grid_7 alpha omega clearfix" style="margin-right: 10px; margin-left: -10px;">{/if}
	{/if}