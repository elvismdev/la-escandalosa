<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_f7c34fc4a48bc683445c1e7bbc245508'] = 'Blok nowych produktów';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_cc68052332d758d991b64087c6e4352e'] = 'Wyświetla blok prezentujący nowo dodane produkty.';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_57af4b7e82ba04c42eb3ac111310a505'] = 'Proszę wypełnić pole \"wyświetlane produkty\".';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_73293a024e644165e9bf48f270af63a0'] = 'Nieprawidłowy numer';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_c888438d14855d7d96a2724ee9c306bd'] = 'Zaktualizowano ustawienia';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_e451e6943bb539d1bd804d2ede6474b5'] = 'Wyświetlane produkty';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_cc4bbebd6a8d2fb633a1dd8ceda3fc8d'] = 'Ilość produktów do wyświetlenia w tym bloku';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_530c88f8210e022b39128e3f0409bbcf'] = 'Zawsze wyświetlaj blok';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Włączone';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_b9f5c797ebbf55adccdd8539a65a0241'] = 'Wyłączone';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_53d61d1ac0507b1bd8cd99db8d64fb19'] = 'Pokaż blok nawet jeżeli żden produkt nie jest dostępny.';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_9ff0635f5737513b1a6f559ac2bff745'] = 'Nowe produkty';
$_MODULE['<{blocknewproducts_mod}prestashop>blocknewproducts_mod_2bc4c1efe10bba9f03fac3c59b4d2ae9'] = 'Brak nowych produktów na tą chwilę';
