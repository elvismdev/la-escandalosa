{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 7457 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
	
{if isset($products)}
	<!-- Products list -->	
	<div id="product_list">
	{foreach from=$products item=product name=products}
		<div class="product_box">
			<div class="product_image">
				{if isset($comparator_max_item) && $comparator_max_item}
					<!-- <input type="checkbox" class="comparator" id="comparator_item_{$product.id_product}" value="comparator_item_{$product.id_product}" {if isset($compareProducts) && in_array($product.id_product, $compareProducts)}checked="checked"{/if}style="opacity: 0; filter: alpha(opacity=0);"/> -->
				{/if}
				{$more_imgs = Product::getProductsImgs($product.id_product)}
				{if count($more_imgs) > 0}
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
						<div class="flipper">
							<div class="front_image">
								<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
							</div>
								<div class="back_image">
									{foreach from=$more_imgs item=second_img}
										<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $second_img.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
									{/foreach}
								</div>
						</div>
					</div>
				{else}
					<div class="flip-container">
						<div>
							<div style="margin-left:2px; width:232px;">
								<a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" /></a>
							</div>
						</div>
					</div>
				{/if}
			</div>
			<div class="product_details">
				<div class="product_title">
					<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">
						<span class="product_manufacturer">{$product.manufacturer_name = Manufacturer::getNameById((int)$product.id_manufacturer)}{$product.manufacturer_name}</span>
						{if isset($product.new) && $product.new == 1}<span class="product_new">{l s='New'}</span>{/if}
						{$product.name|escape:'htmlall':'UTF-8'|truncate:250:'...'}
					</a>
				</div>
				<div class="product_desc">
					<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.description_short|strip_tags|truncate:600:'...'}</a>
				</div>
				<div>
					{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="product_price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>
						{if isset($product.reduction) && $product.reduction}<span class="product_discount">{convertPrice price=$product.price_without_reduction}</span>{/if}
						<br />
					{/if}
					{if isset($product.online_only) && $product.online_only}<span class="online_only">{l s='Online only!'}</span>{/if}
					{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="product_on_sale">{l s='On sale!'}</span>{/if}
					{/if}
				</div>
			</div>
			{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
					{if ($product.allow_oosp || $product.quantity > 0)}
						{if isset($static_token)}
							<div class="hidden_cart"><a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a></div>
						{else}
							<div class="hidden_cart"><a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add&amp;id_product={$product.id_product|intval}", false)}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a></div>
						{/if}						
					{else}
						<div class="hidden_cart"><a href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}"><span class="exclusive">{l s='View'}</span></a></div>
					{/if}
				{/if}
		</div>
	{/foreach}
	</div>
	<!-- /Products list -->
{/if}
<script type="text/javascript">

jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};

$(function() {
	var cc = $.cookie('list_grid');
	if (cc == 'g') {
		$('#product_list').addClass('grid');
		$('input[type="checkbox"]').ezMark();
	} else {
		$('#product_list').removeClass('grid');
		$('input[type="checkbox"]').ezMark();
	}
});
$(document).ready(function() {

	$('#grid').click(function() {
		$('#product_list').fadeOut(300, function() {
			$(this).addClass('grid').fadeIn(300);
			$.cookie('list_grid', 'g');
		});
		return false;
	});
	
	$('#list').click(function() {
		$('#product_list').fadeOut(300, function() {
			$(this).removeClass('grid').fadeIn(300);
			$.cookie('list_grid', null);
		});
		return false;
	});

});
</script>
