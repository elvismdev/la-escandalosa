{*
* 2007-2012 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 8337 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($categoryProducts) > 0 && $categoryProducts !== false}<div class="acc_head_3"><span>{l s='the same category' mod='productscategory'}</span><div class="cool">{l s='the same category' mod='productscategory'}</div><span>{l s='the same category' mod='productscategory'}</span></div>
<div class="infiniteCarousel">
<div class="flexslider carousel">
		<ul class="slides">
			{foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
			<li class="product_box_carousel">
				{$more_imgs = Product::getProductsImgs($categoryProduct.id_product)}
				{if count($more_imgs) > 0}
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');" style="margin-bottom: 10px; margin-top: 10px;">
						<div class="flipper">
							<div class="front_image">
								<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" title="{$categoryProduct.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>
							</div>
								<div class="back_image">
									{foreach from=$more_imgs item=second_img}
										<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" title="{$categoryProduct.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($categoryProduct.link_rewrite, $second_img.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>
									{/foreach}
								</div>
						</div>
					</div>
				{else}
					<div class="flip-container">
						<div>
							<div style="margin-left:2px; width:232px;">
								<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" title="{$categoryProduct.name|escape:html:'UTF-8'}" class="product_image"><img src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'home_default')}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>
							</div>
						</div>
					</div>
				{/if}
				<div class="product_title_carousel">
					<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" title="{$categoryProduct.name|htmlspecialchars}"><span class="product_manufacturer">{$categoryProduct.manufacturer_name = Manufacturer::getNameById((int)$categoryProduct.id_manufacturer)}{$categoryProduct.manufacturer_name}</span>{if isset($categoryProduct.new) && $categoryProduct.new == 1}<span class="product_new">{l s='Nuevo' mod='productscategory'}</span>{/if}{$categoryProduct.name}</a>
				</div>
				<div class="product_price_carousel">
				{if (!$PS_CATALOG_MODE AND ((isset($categoryProduct.show_price) && $categoryProduct.show_price) || (isset($categoryProduct.available_for_order) && $categoryProduct.available_for_order)))}
					{if isset($categoryProduct.show_price) && $categoryProduct.show_price && !isset($restricted_country_mode)}
						<span class="product_price" style="display: inline;">
							{if !$priceDisplay}
								{convertPrice price=$categoryProduct.price}
							{else}
								{convertPrice price=$categoryProduct.price_tax_exc}
							{/if}
						</span>
						{if isset($categoryProduct.reduction) && $categoryProduct.reduction}
							<span class="product_discount">{convertPrice price=$categoryProduct.price_without_reduction}</span>
						{/if}
						<br />
					{/if}
					{if isset($categoryProduct.online_only) && $categoryProduct.online_only}<span class="online_only">{l s='Online only!'}</span>{/if}
				{/if}
				{if $ProdDisplayPrice AND $categoryProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
					{if isset($categoryProduct.on_sale) && $categoryProduct.on_sale && isset($categoryProduct.show_price) && $categoryProduct.show_price && !$PS_CATALOG_MODE}
						<span class="product_on_sale">{l s='On sale!'}</span>
					{/if}
				{else}
					<br />
				{/if}
				</div>
			</li>
			{/foreach}
		</ul>

	</div>

</div>
	<script type="text/javascript">
		$('#productscategory_list').trigger('goto', [{$middlePosition}-3]);
	</script>
{/if}
