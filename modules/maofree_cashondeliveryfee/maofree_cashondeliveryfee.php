<?php

/*Copyright 2011 maofree  email: msecco@gmx.com  website: http://www.maofree-developer.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 3, as
published by the Free Software Foundation.

This file can't be removed. This module can't be sold.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

if (!defined('_PS_VERSION_'))
	exit;

class Maofree_CashOnDeliveryFee extends PaymentModule
{
	public function __construct()
	{
		$this->name = 'maofree_cashondeliveryfee';
		$this->tab = 'payments_gateways';
		$this->version = '1.0';
		$this->author = 'maofree';

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		parent::__construct();

		$this->displayName = $this->l('Cash on delivery (COD)');
		$this->description = $this->l('Accept cash on delivery payments with extra fee');
		$this->confirmUninstall = $this->l('Are you sure you want to delete your details?');
	}

	public function install()
	{
		if
		(
		   !parent::install() ||
			!$this->registerHook('payment') ||
			!$this->registerHook('paymentReturn') ||
		   !Configuration::updateValue('DELIVERY_FEE', 0) ||
		   !Configuration::updateValue('FEE_TYPE', 'Percent') ||
		   !Configuration::updateValue('FEE_MIN', 0) ||
		   !Configuration::updateValue('MAX_CARTVALUE_ACCEPTABLE', 0) ||
		   !Configuration::updateValue('LIMIT_CARTVALUE', 0) ||
		   !Configuration::updateValue('LIST_CARRIERS', '')
		)
			return false;

		return true;
	}
	public function uninstall()
	{
		if
		(
		   !Configuration::deleteByName('DELIVERY_FEE') ||
		   !Configuration::deleteByName('FEE_TYPE') ||
		   !Configuration::deleteByName('FEE_MIN') ||
		   !Configuration::deleteByName('MAX_CARTVALUE_ACCEPTABLE') ||
		   !Configuration::deleteByName('LIMIT_CARTVALUE') ||
		   !Configuration::deleteByName('LIST_CARRIERS') ||
		   !parent::uninstall()
		)
			return false;

		return true;
	}

	public function getContent()
	{
		$output = '<style type="text/css">label { width: 550px } .wiki { color: red; font-size: 130%; font-weight: bold; margin: 10px 0; display: block; width: 200px }</style>';
		$output .= '<h2>'.$this->displayName.'</h2>';
		$errors = array();

		if (Tools::isSubmit('submitMAOCOD'))
		{
			$fee = Tools::getValue('fee');
			$feetype = Tools::getValue('feetype');
			$feemin = Tools::getValue('feemin');
			$maxcartvalueacceptable = Tools::getValue('maxcartvalueacceptable');
			$limitcartvalue = Tools::getValue('limitcartvalue');
			$carriersid = Tools::getValue('carrierslist');

			if ($fee < 0 OR !Validate::isUnsignedFloat($fee))
				$errors[] = $this->l('Invalid number in = Percent or amount of fee');
         elseif($fee)
				Configuration::updateValue('DELIVERY_FEE', (float)$fee);
			else
				Configuration::updateValue('DELIVERY_FEE', 0);
			if ($feemin < 0 OR !Validate::isUnsignedFloat($feemin))
				$errors[] = $this->l('Invalid number in = Fee base amount');
         elseif($feemin)
				Configuration::updateValue('FEE_MIN', (float)$feemin);
			else
				Configuration::updateValue('FEE_MIN', 0);
			if ($maxcartvalueacceptable < 0 OR !Validate::isUnsignedFloat($maxcartvalueacceptable))
				$errors[] = $this->l('Invalid number in = Max cart value acceptable with this paymnent form');
         elseif($maxcartvalueacceptable)
				Configuration::updateValue('MAX_CARTVALUE_ACCEPTABLE', (float)$maxcartvalueacceptable);
			else
				Configuration::updateValue('MAX_CARTVALUE_ACCEPTABLE', 0);
			if ($limitcartvalue < 0 OR !Validate::isUnsignedFloat($limitcartvalue))
				$errors[] = $this->l('Invalid number in = Max cart value to apply the fee base amount');
         elseif($limitcartvalue)
				Configuration::updateValue('LIMIT_CARTVALUE', (float)$limitcartvalue);
			else
				Configuration::updateValue('LIMIT_CARTVALUE', 0);
			if (!empty($carriersid))
				Configuration::updateValue('LIST_CARRIERS', implode(',',$carriersid));
			Configuration::updateValue('FEE_TYPE', $feetype);

			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
			   $output .= $this->displayConfirmation($this->l('Settings updated'));
		}

		return $output.$this->displayForm();
	}

	private function displayForm()
	{
		global $cookie;

		$output = '
		<fieldset>
			<legend><img src="'.$this->_path.'img/mylogo.gif" alt="maofree\'s module" title="maofree\'s module" />maofree</legend>
			<a href="http://www.maofree-developer.com" target="_blank"><img src="'._MODULE_DIR_.$this->name.'/img/donate.png" alt="maofree\'s website" title="'.$this->l('Do you need some help? (click here)').'" /></a>
			<div style="float:right;width:70%;">
				<h3 style="color:lightCoral;">'.$this->l('If you like my job, you could support me with a donation').'.</h3>
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="hosted_button_id" value="MEF3Z7XDHQZF8">
					<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="Paypal" style="margin-top:20px;">
					<img alt="" border="0" src="https://www.paypal.com/it_IT/i/scr/pixel.gif" width="1" height="1">
				</form>
			</div>
		</fieldset>
		<br />
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset>
				<legend><img src="'.$this->_path.'img/mylogo.gif" alt="maofree\'s module" title="maofree\'s module" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Percent or amount of fee').':</label>
				<div class="margin-form">
					<input type="text" size="10" maxlength="10" name="fee" value="'.Tools::getValue('fee', Configuration::get('DELIVERY_FEE')).'" />
					'.$this->l('(eg. 2)').'
				</div><br />
				<label>'.$this->l('Type of fee').':</label>
				<div class="margin-form">
					<select name="feetype">
						<option value="Percent" '.(Tools::getValue('feetype', Configuration::get('FEE_TYPE')) == 'Percent' ? 'selected="selected"' : '').'>Percent</option>
						<option value="Amount" '.(Tools::getValue('feetype', Configuration::get('FEE_TYPE')) == 'Amount' ? 'selected="selected"' : '').'>Amount</option>
					</select>
				</div><br />
				<label>'.$this->l('Fee base amount').':</label>
				<div class="margin-form">
					<input type="text" size="10" maxlength="10" name="feemin" value="'.Tools::getValue('feemin', Configuration::get('FEE_MIN')).'" />
					'.$this->l('(eg. 10)').'
				</div><br />
				<label>'.$this->l('Max cart value to apply the fee base amount').':</label>
				<div class="margin-form">
				   <input type="text" size="10" maxlength="10" name="limitcartvalue" value="'.Tools::getValue('limitcartvalue', Configuration::get('LIMIT_CARTVALUE')).'" />
               '.$this->l('(eg. 500)').'
				</div><br />
				<label>'.$this->l('Max cart value acceptable with this paymnent form').':</label>
				<div class="margin-form">
				   <input type="text" size="10" maxlength="10" name="maxcartvalueacceptable" value="'.Tools::getValue('maxcartvalueacceptable', Configuration::get('MAX_CARTVALUE_ACCEPTABLE')).'" />
               '.$this->l('(eg. 2000), no limit with 0').'
					<p class="clear">'.$this->l('exceeded this limit, this form of payment will not appear in the choice of payments').'.</p>
				</div><br />
				<label>'.$this->l('Set which carriers you want to enable for this payment method').':</label>
	         <div class="margin-form">
		         <select name="carrierslist[]" multiple="multiple" style="height: 120px">';

      $carriersstringid = Configuration::get('LIST_CARRIERS');
		if (!empty($carriersstringid))
	      $checkedcarriers = explode(",", $carriersstringid);
		else
			$checkedcarriers = array();
      $carriers = Carrier::getCarriers((int)($cookie->id_lang), true, false, false);
	   foreach ($carriers AS $row)
			$output .= '<option value="'.$row['id_carrier'].'" '.(in_array($row['id_carrier'],$checkedcarriers) ? 'selected="selected"' : '').'>'.$row['name'].'</option>';

		$output .= '
			      </select> '.$this->l('(Hold CTRL to select multiples)').'
	         </div><br />
				<div class="margin-form clear"><input type="submit" name="submitMAOCOD" value="'.$this->l('Update settings').'" class="button" /></div>
			</fieldset>
		</form>
		<br />
		<fieldset class="hint clear" style="display: block;">
			<legend><img src="'.$this->_path.'img/mylogo.gif" alt="maofree\'s module" title="maofree\'s module" />'.$this->l('Instructions:').'</legend>
			<h3>1) '.$this->l('NO extra configuration is necessary to enable this form of payment').':</h3>
			<p>
				'.$this->l('It is not necessary to add any new order status from Order Status page or any line in defines.inc.php').'.<br />
				'.$this->l('This module uses "Preparation Status", so the order after the purchase, will be in "Preparation in Progress"').'.
         </p>
		</fieldset>
		<br />
		<fieldset class="hint clear" style="display: block;">
			<legend><img src="'.$this->_path.'img/mylogo.gif" alt="maofree\'s module" title="maofree\'s module" />'.$this->l('Right configuration for your website:').'</legend>
			<h3>1) '.$this->l('Write permissions').':</h3>
			<p>'.$this->l('While you have your FTP connected to your Web hosting server, make sure the following PrestaShop folders have write permissions (also known as "CHMOD 755") but do not apply these permissions recursively (to their subfolders "CHMOD -R 755"): /config, /upload, /download, /tools/smarty/compile. Then make sure the following folders have write permissions and apply these permissions recursively (to their subfolders): /img, /mails, /modules, /themes/prestashop/lang, /translations').'.</p>
			<p>'.$this->l('On many newer servers, 755 permissions will suffice and 777 will not work (as it is less secure), however, if you experience trouble when using 755, try to change to 777').'.</p>
			<p>'.$this->l('To have more knowledge on this argument, look at these pages below').'.</p>
			<a class="wiki" href="http://www.prestashop.com/wiki/Installing_And_Updating_PrestaShop_Software/" target="_blank" alt="Prestashop Wiki" title="Prestashop Wiki">Prestashop Wiki</a>
			<a class="wiki" href="http://codex.wordpress.org/Changing_File_Permissions" target="_blank" alt="Wordpress Wiki" title="Wordpress Wiki">Wordpress Wiki</a>
			<a class="wiki" href="http://www.youtube.com/watch?v=oq0oM2w9lcQ" target="_blank" alt="Youtube Video" title="Youtube Video">Youtube Video</a>
			<h3>2) '.$this->l('When you do some tests').':</h3>
			<p><span style="color: orangered">'.$this->l('Use force-compile enabled').'.</span><br />
				<span style="color: orangered">'.$this->l('In config/config.inc.php write display_errors on and _PS_DEBUG_SQL_ true').'.</span><br />
				'.$this->l('If you have some problem with smarty, go in tools/smarty/compile/ and delete all files except the index.php file').'.<br />
	         '.$this->l('After these tests return to the default configuration (do not use these settings for production)').'.
	      </p>
		</fieldset>';

		return $output;
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return ;

		global $smarty;

		// Check if cart has product download
		foreach ($params['cart']->getProducts() AS $product)
		{
			$pd = ProductDownload::getIdFromIdProduct((int)($product['id_product']));
			if ($pd AND Validate::isUnsignedInt($pd))
				return false;
		}

	   $smarty->assign(array(
			'COD_fee' => (float)($this->getCostCOD($params, 1)),
	      'COD_this_path' => $this->_path,
	      'COD_this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
	   ));

	   $paymentmethod = $this->display(__FILE__, 'payment.tpl');

		$cart = $params['cart'];
		if ($cart) {
			$id_carrier = (string)$cart->id_carrier;
         if(strpos(Configuration::get('LIST_CARRIERS'), $id_carrier) === FALSE)
            $paymentmethod = '';
	      $verifytotalorder = (float)Configuration::get('MAX_CARTVALUE_ACCEPTABLE');
         if($verifytotalorder > 0) {
				$totalorder = (float)$cart->getOrderTotal(true, Cart::BOTH);
	         if($totalorder > $verifytotalorder)
	            $paymentmethod = '';
	      }
		}

		return $paymentmethod;
	}

	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return ;

		global $smarty;

		$smarty->assign('COD_this_path', $this->_path);

		return $this->display(__FILE__, 'confirmation.tpl');
	}

	public function getCostCOD($data, $k)
	{
		if($k==1)
		{
			$cartvalue = (float)($data['cart']->getOrderTotal(true, Cart::BOTH));
			$currency = (int)($data['cart']->id_currency);
      }
		elseif($k==2)
		{
		   $cartvalue = (float)($data->getOrderTotal(true, Cart::BOTH));
			$currency = (int)($data->id_currency);
      }
		$minimalfee = Tools::convertPrice((float)(Configuration::get('FEE_MIN')), $currency);
		$minimalfeecartvalue = Tools::convertPrice((float)(Configuration::get('LIMIT_CARTVALUE')), $currency);
		if(Configuration::get('FEE_TYPE')=='Amount')
		{
			if($cartvalue > $minimalfeecartvalue)
				return Tools::convertPrice((float)(Configuration::get('DELIVERY_FEE')), $currency);
			else
				return $minimalfee;
		}
		else
		{
			if($cartvalue > $minimalfeecartvalue)
			{
				$percent = (float)(Configuration::get('DELIVERY_FEE'));
				$percent = $percent / 100;
				$fee = $cartvalue * $percent;
   			return $fee;
   		}
   		else
				return $minimalfee;
		}
	}

	public function validateOrderCOD($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = NULL, $extraVars = array(), $currency_special = NULL, $dont_touch_amount = false, $secure_key = false)
	{
		global $cart;

		$cart = new Cart((int)($id_cart));

		$fee = (float)($this->getCostCOD($cart, 2));

		// Does order already exists ?
		if (Validate::isLoadedObject($cart) AND $cart->OrderExists() == false)
		{
			if ($secure_key !== false AND $secure_key != $cart->secure_key)
				die(Tools::displayError());
			// Copying data from cart
			$order = new Order();
            $order->reference = $order->generateReference();
            $order->id_shop = $order->id_shop_group = (int)Context::getContext()->shop->id;
            $order->id_carrier = (int)($cart->id_carrier);
			$order->id_customer = (int)($cart->id_customer);
			$order->id_address_invoice = (int)($cart->id_address_invoice);
			$order->id_address_delivery = (int)($cart->id_address_delivery);
			$vat_address = new Address((int)($order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
			$order->id_currency = ($currency_special ? (int)($currency_special) : (int)($cart->id_currency));
			$order->id_lang = (int)($cart->id_lang);
			$order->id_cart = (int)($cart->id);
			$customer = new Customer((int)($order->id_customer));
			$order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($customer->secure_key));
			$order->payment = $paymentMethod;
			if (isset($this->name))
				$order->module = $this->name;
			$order->recyclable = $cart->recyclable;
			$order->gift = (int)($cart->gift);
			$order->gift_message = $cart->gift_message;
			$currency = new Currency($order->id_currency);
			$order->conversion_rate = $currency->conversion_rate;
			$amountPaid = !$dont_touch_amount ? Tools::ps_round((float)($amountPaid)+$fee, 2) : ((float)($amountPaid)+$fee);
			$order->total_paid_real = $amountPaid;
			$order->total_products = (float)($cart->getOrderTotal(false, Cart::ONLY_PRODUCTS));
			$order->total_products_wt = (float)($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS));
			$order->total_discounts = (float)(abs($cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS)));
			$order->total_shipping = (float)($cart->getOrderShippingCost())+$fee;
			$order->carrier_tax_rate = (float)Tax::getCarrierTaxRate($cart->id_carrier, (int)$cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
			$order->total_wrapping = (float)(abs($cart->getOrderTotal(true, Cart::ONLY_WRAPPING)));
			$order->total_paid = (float)(Tools::ps_round((float)($cart->getOrderTotal(true, Cart::BOTH))+$fee, 2));
            $order->total_paid_tax_incl = $order->total_paid;
            $order->total_paid_tax_excl = (float)(Tools::ps_round((float)($cart->getOrderTotal(true, Cart::BOTH)), 2));
			$order->invoice_date = '0000-00-00 00:00:00';
			$order->delivery_date = '0000-00-00 00:00:00';
			// Amount paid by customer is not the right one -> Status = payment error
			// We don't use the following condition to avoid the float precision issues : http://www.php.net/manual/en/language.types.float.php
			// if ($order->total_paid != $order->total_paid_real)
			// We use number_format in order to compare two string
			if (number_format($order->total_paid, 2) != number_format($order->total_paid_real, 2))
				$id_order_state = Configuration::get('PS_OS_ERROR');

			// Creating order
			if ($cart->OrderExists() == false)
				$result = $order->add();
			else
			{
				$errorMessage = Tools::displayError('An order has already been placed using this cart.');
				Logger::addLog($errorMessage, 4, '0000001', 'Cart', intval($order->id_cart));
				die($errorMessage);
			}

			// Next !
			if ($result AND isset($order->id))
			{
				if (!$secure_key)
					$message .= $this->l('Warning : the secure key is empty, check your payment account before validation');
				// Optional message to attach to this order
				if (isset($message) AND !empty($message))
				{
					$msg = new Message();
					$message = strip_tags($message, '<br>');
					if (Validate::isCleanHtml($message))
               {
						$msg->message = $message;
						$msg->id_order = intval($order->id);
						$msg->private = 1;
						$msg->add();
					}
				}

				// Insert products from cart into order_detail table
				$products = $cart->getProducts();
				$productsList = '';
				$db = Db::getInstance();
				$query = 'INSERT INTO `'._DB_PREFIX_.'order_detail`
					(`id_order`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_price`, `reduction_percent`, `reduction_amount`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_upc`, `product_reference`, `product_supplier_reference`, `product_weight`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_deadline`, `download_hash`, `unit_price_tax_incl`)
				VALUES ';

				$customizedDatas = Product::getAllCustomizedDatas((int)($order->id_cart));
				Product::addCustomizationPrice($products, $customizedDatas);
				$outOfStock = false;

				$storeAllTaxes = array();

				foreach ($products AS $key => $product)
				{
					$productQuantity = (int)(Product::getQuantity((int)($product['id_product']), ($product['id_product_attribute'] ? (int)($product['id_product_attribute']) : NULL)));
					$quantityInStock = ($productQuantity - (int)($product['cart_quantity']) < 0) ? $productQuantity : (int)($product['cart_quantity']);
					if ($id_order_state != Configuration::get('PS_OS_CANCELED') AND $id_order_state != Configuration::get('PS_OS_ERROR'))
					{
						if (Product::updateQuantity($product, (int)$order->id))
							$product['stock_quantity'] -= $product['cart_quantity'];

						if ($product['stock_quantity'] < 0 && Configuration::get('PS_STOCK_MANAGEMENT'))
							$outOfStock = true;

						Product::updateDefaultAttribute($product['id_product']);
					}
					$price = Product::getPriceStatic((int)($product['id_product']), false, ($product['id_product_attribute'] ? (int)($product['id_product_attribute']) : NULL), 6, NULL, false, true, $product['cart_quantity'], false, (int)($order->id_customer), (int)($order->id_cart), (int)($order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
					$price_wt = Product::getPriceStatic((int)($product['id_product']), true, ($product['id_product_attribute'] ? (int)($product['id_product_attribute']) : NULL), 2, NULL, false, true, $product['cart_quantity'], false, (int)($order->id_customer), (int)($order->id_cart), (int)($order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));

					/* Store tax info */
					$id_country = (int)Country::getDefaultCountryId();
					$id_state = 0;
					$id_county = 0;
					$rate = 0;
					$id_address = $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
					$address_infos = Address::getCountryAndState($id_address);
					if ($address_infos['id_country'])
					{
						$id_country = (int)($address_infos['id_country']);
						$id_state = (int)$address_infos['id_state'];
						$id_county = (int)County::getIdCountyByZipCode($address_infos['id_state'], $address_infos['postcode']);
					}
					$allTaxes = TaxRulesGroup::getTaxes((int)Product::getIdTaxRulesGroupByIdProduct((int)$product['id_product']), $id_country, $id_state, $id_county);
					$nTax = 0;
					foreach ($allTaxes AS $res)
					{
						if (!isset($storeAllTaxes[$res->id]))
							$storeAllTaxes[$res->id] = array();
						$storeAllTaxes[$res->id]['name'] = $res->name[(int)$order->id_lang];
						$storeAllTaxes[$res->id]['rate'] = $res->rate;

						if (!$nTax++)
							$storeAllTaxes[$res->id]['amount'] = ($price * (1 + ($res->rate * 0.01))) - $price;
						else
						{
							$priceTmp = $price_wt / (1 + ($res->rate * 0.01));
							$storeAllTaxes[$res->id]['amount'] = $price_wt - $priceTmp;
						}
					}
					/* End */

					// Add some informations for virtual products
					$deadline = '0000-00-00 00:00:00';
					$download_hash = NULL;
					if ($id_product_download = ProductDownload::getIdFromIdProduct((int)($product['id_product'])))
					{
						$productDownload = new ProductDownload((int)($id_product_download));
						$deadline = $productDownload->getDeadLine();
						$download_hash = $productDownload->getHash();
					}

					// Exclude VAT
					if (Tax::excludeTaxeOption())
					{
						$product['tax'] = 0;
						$product['rate'] = 0;
						$tax_rate = 0;
					}
					else
						$tax_rate = Tax::getProductTaxRate((int)($product['id_product']), $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

               $ecotaxTaxRate = 0;
               if (!empty($product['ecotax']))
                  $ecotaxTaxRate = Tax::getProductEcotaxRate($order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

               $product_price = (float)Product::getPriceStatic((int)($product['id_product']), false, ($product['id_product_attribute'] ? (int)($product['id_product_attribute']) : NULL), (Product::getTaxCalculationMethod((int)($order->id_customer)) == PS_TAX_EXC ? 2 : 6), NULL, false, false, $product['cart_quantity'], false, (int)($order->id_customer), (int)($order->id_cart), (int)($order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}), $specificPrice, false, false);

					$group_reduction = (float)GroupReduction::getValueForProduct((int)$product['id_product'], $customer->id_default_group) * 100;
					if (!$group_reduction)
						$group_reduction = Group::getReduction((int)$order->id_customer);

					$quantityDiscount = SpecificPrice::getQuantityDiscount((int)$product['id_product'], Shop::getCurrentShop(), (int)$cart->id_currency, (int)$vat_address->id_country, (int)$customer->id_default_group, (int)$product['cart_quantity']);
					$unitPrice = Product::getPriceStatic((int)$product['id_product'], true, ($product['id_product_attribute'] ? intval($product['id_product_attribute']) : NULL), 2, NULL, false, true, 1, false, (int)$order->id_customer, NULL, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
					$quantityDiscountValue = $quantityDiscount ? ((Product::getTaxCalculationMethod((int)$order->id_customer) == PS_TAX_EXC ? Tools::ps_round($unitPrice, 2) : $unitPrice) - $quantityDiscount['price'] * (1 + $tax_rate / 100)) : 0.00;
					$query .= '('.(int)($order->id).',
						'.(int)($product['id_product']).',
						'.(isset($product['id_product_attribute']) ? (int)($product['id_product_attribute']) : 'NULL').',
						\''.pSQL($product['name'].((isset($product['attributes']) AND $product['attributes'] != NULL) ? ' - '.$product['attributes'] : '')).'\',
						'.(int)($product['cart_quantity']).',
						'.$quantityInStock.',
						'.$product_price.',
						'.(float)(($specificPrice AND $specificPrice['reduction_type'] == 'percentage') ? $specificPrice['reduction'] * 100 : 0.00).',
						'.(float)(($specificPrice AND $specificPrice['reduction_type'] == 'amount') ? (!$specificPrice['id_currency'] ? Tools::convertPrice($specificPrice['reduction'], $order->id_currency) : $specificPrice['reduction']) : 0.00).',
						'.$group_reduction.',
						'.$quantityDiscountValue.',
						'.(empty($product['ean13']) ? 'NULL' : '\''.pSQL($product['ean13']).'\'').',
						'.(empty($product['upc']) ? 'NULL' : '\''.pSQL($product['upc']).'\'').',
						'.(empty($product['reference']) ? 'NULL' : '\''.pSQL($product['reference']).'\'').',
						'.(empty($product['supplier_reference']) ? 'NULL' : '\''.pSQL($product['supplier_reference']).'\'').',
						'.(float)($product['id_product_attribute'] ? $product['weight_attribute'] : $product['weight']).',
						\''.(empty($tax_rate) ? '' : pSQL($product['tax'])).'\',
						'.(float)($tax_rate).',
						'.(float)Tools::convertPrice(floatval($product['ecotax']), intval($order->id_currency)).',
						'.(float)$ecotaxTaxRate.',
						'.(($specificPrice AND $specificPrice['from_quantity'] > 1) ? 1 : 0).',
						\''.pSQL($deadline).'\',
						\''.pSQL($download_hash).'\',
                        \''.(float)Tools::convertPrice(floatval($product['price_wt']), intval($order->id_currency)).'\'),';

					$customizationQuantity = 0;
					if (isset($customizedDatas[$product['id_product']][$product['id_product_attribute']]))
					{
						$customizationText = '';
						foreach ($customizedDatas[$product['id_product']][$product['id_product_attribute']] AS $customization)
						{
							if (isset($customization['datas'][_CUSTOMIZE_TEXTFIELD_]))
								foreach ($customization['datas'][_CUSTOMIZE_TEXTFIELD_] AS $text)
									$customizationText .= $text['name'].':'.' '.$text['value'].'<br />';

							if (isset($customization['datas'][_CUSTOMIZE_FILE_]))
								$customizationText .= sizeof($customization['datas'][_CUSTOMIZE_FILE_]) .' '. Tools::displayError('image(s)').'<br />';

							$customizationText .= '---<br />';
						}

						$customizationText = rtrim($customizationText, '---<br />');

						$customizationQuantity = (int)($product['customizationQuantityTotal']);
						$productsList .=
						'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
							<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
							<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').' - '.$this->l('Customized').(!empty($customizationText) ? ' - '.$customizationText : '').'</strong></td>
							<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $price : $price_wt, $currency, false).'</td>
							<td style="padding: 0.6em 0.4em; text-align: center;">'.$customizationQuantity.'</td>
							<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice($customizationQuantity * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? $price : $price_wt), $currency, false).'</td>
						</tr>';
					}

					if (!$customizationQuantity OR (int)$product['cart_quantity'] > $customizationQuantity)
						$productsList .=
						'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
							<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
							<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').'</strong></td>
							<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $price : $price_wt, $currency, false).'</td>
							<td style="padding: 0.6em 0.4em; text-align: center;">'.((int)($product['cart_quantity']) - $customizationQuantity).'</td>
							<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(((int)($product['cart_quantity']) - $customizationQuantity) * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? $price : $price_wt), $currency, false).'</td>
						</tr>';
				} // end foreach ($products)
				$query = rtrim($query, ',');
				$result = $db->Execute($query);

				/* Add carrier tax */
				$shippingCostTaxExcl = $cart->getOrderShippingCost((int)$order->id_carrier, false);
				$allTaxes = TaxRulesGroup::getTaxes((int)Carrier::getIdTaxRulesGroupByIdCarrier((int)$order->id_carrier), $id_country, $id_state, $id_county);
				$nTax = 0;

				foreach ($allTaxes AS $res)
				{
					if (!isset($res->id))
						continue;

					if (!isset($storeAllTaxes[$res->id]))
						$storeAllTaxes[$res->id] = array();
					if (!isset($storeAllTaxes[$res->id]['amount']))
						$storeAllTaxes[$res->id]['amount'] = 0;
					$storeAllTaxes[$res->id]['name'] = $res->name[(int)$order->id_lang];
					$storeAllTaxes[$res->id]['rate'] = $res->rate;

					if (!$nTax++)
						$storeAllTaxes[$res->id]['amount'] += ($shippingCostTaxExcl * (1 + ($res->rate * 0.01))) - $shippingCostTaxExcl;
					else
					{
						$priceTmp = $order->total_shipping / (1 + ($res->rate * 0.01));
						$storeAllTaxes[$res->id]['amount'] += $order->total_shipping - $priceTmp;
					}
				}

				/* Store taxes */
				foreach ($storeAllTaxes AS $t)
					Db::getInstance()->Execute('
					INSERT INTO '._DB_PREFIX_.'order_tax (id_order, tax_name, tax_rate, amount)
					VALUES ('.(int)$order->id.', \''.pSQL($t['name']).'\', \''.(float)($t['rate']).'\', '.(float)$t['amount'].')');

				// Insert discounts from cart into order_discount table
				$discounts = $cart->getDiscounts();
				$discountsList = '';
				$total_discount_value = 0;
				$shrunk = false;
				foreach ($discounts AS $discount)
				{
					$objDiscount = new Discount((int)$discount['id_discount']);
					$value = $objDiscount->getValue(sizeof($discounts), $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS), $order->total_shipping, $cart->id);
					if ($objDiscount->id_discount_type == 2 AND in_array($objDiscount->behavior_not_exhausted, array(1,2)))
						$shrunk = true;

					if ($shrunk AND ($total_discount_value + $value) > ($order->total_products_wt + $order->total_shipping + $order->total_wrapping))
					{
						$amount_to_add = ($order->total_products_wt + $order->total_shipping + $order->total_wrapping) - $total_discount_value;
						if ($objDiscount->id_discount_type == 2 AND $objDiscount->behavior_not_exhausted == 2)
						{
							$voucher = new Discount();
							foreach ($objDiscount AS $key => $discountValue)
								$voucher->$key = $discountValue;
							$voucher->name = 'VSRK'.(int)$order->id_customer.'O'.(int)$order->id;
							$voucher->value = (float)$value - $amount_to_add;
							$voucher->add();
							$params['{voucher_amount}'] = Tools::displayPrice($voucher->value, $currency, false);
							$params['{voucher_num}'] = $voucher->name;
							$params['{firstname}'] = $customer->firstname;
							$params['{lastname}'] = $customer->lastname;
							$params['{id_order}'] = $order->id;
							@Mail::Send((int)$order->id_lang, 'voucher', Mail::l('New voucher regarding your order #').$order->id, $params, $customer->email, $customer->firstname.' '.$customer->lastname);
						}
					}
					else
						$amount_to_add = $value;
					$order->addDiscount($objDiscount->id, $objDiscount->name, $amount_to_add);
					$total_discount_value += $amount_to_add;
					if ($id_order_state != Configuration::get('PS_OS_ERROR') AND $id_order_state != Configuration::get('PS_OS_CANCELED'))
						$objDiscount->quantity = $objDiscount->quantity - 1;
					$objDiscount->update();

					$discountsList .=
					'<tr style="background-color:#EBECEE;">
							<td colspan="4" style="padding: 0.6em 0.4em; text-align: right;">'.$this->l('Voucher code:').' '.$objDiscount->name.'</td>
							<td style="padding: 0.6em 0.4em; text-align: right;">'.($value != 0.00 ? '-' : '').Tools::displayPrice($value, $currency, false).'</td>
					</tr>';
				}

				// Specify order id for message
				$oldMessage = Message::getMessageByCartId((int)($cart->id));
				if ($oldMessage)
				{
					$message = new Message((int)$oldMessage['id_message']);
					$message->id_order = (int)$order->id;
					$message->update();
				}

				// Hook new order
				$orderStatus = new OrderState((int)$id_order_state, (int)$order->id_lang);
				if (Validate::isLoadedObject($orderStatus))
				{
                    // > 1.5 Hook
                    Module::hookExec('actionValidateOrder', array('order' => $order, 'customer' => $customer, 'cart' => $cart, 'currency' => $currency));
                    // < 1.5 Hook
					Hook::newOrder($cart, $order, $customer, $currency, $orderStatus);
					foreach ($cart->getProducts() AS $product)
						if ($orderStatus->logable) {
							ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
                        }
				}

				if (isset($outOfStock) && $outOfStock && Configuration::get('PS_STOCK_MANAGEMENT'))
				{
					$history = new OrderHistory();
					$history->id_order = (int)$order->id;
					$history->changeIdOrderState(Configuration::get('PS_OS_OUTOFSTOCK'), (int)$order->id);
					$history->addWithemail();
				}

				// Set order state in order history ONLY even if the "out of stock" status has not been yet reached
				// So you migth have two order states
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState((int)$id_order_state, (int)$order->id);
				$new_history->addWithemail(true, $extraVars);

				// Order is reloaded because the status just changed
				$order = new Order($order->id);

				// Send an e-mail to customer
				if ($id_order_state != Configuration::get('PS_OS_ERROR') AND $id_order_state != Configuration::get('PS_OS_CANCELED') AND $customer->id)
				{
					$invoice = new Address((int)($order->id_address_invoice));
					$delivery = new Address((int)($order->id_address_delivery));
					$carrier = new Carrier((int)($order->id_carrier), $order->id_lang);
					$delivery_state = $delivery->id_state ? new State((int)($delivery->id_state)) : false;
					$invoice_state = $invoice->id_state ? new State((int)($invoice->id_state)) : false;

					$data = array(
					'{firstname}' => $customer->firstname,
					'{lastname}' => $customer->lastname,
					'{email}' => $customer->email,
					'{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
					'{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
					'{delivery_block_html}' => $this->_getFormatedAddress($delivery, "<br />",
					array(
						'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>',
						'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>')),
					'{invoice_block_html}' => $this->_getFormatedAddress($invoice, "<br />",
					array(
						'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>',
						'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>')),
					'{delivery_company}' => $delivery->company,
					'{delivery_firstname}' => $delivery->firstname,
					'{delivery_lastname}' => $delivery->lastname,
					'{delivery_address1}' => $delivery->address1,
					'{delivery_address2}' => $delivery->address2,
					'{delivery_city}' => $delivery->city,
					'{delivery_postal_code}' => $delivery->postcode,
					'{delivery_country}' => $delivery->country,
					'{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
					'{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
					'{delivery_other}' => $delivery->other,
					'{invoice_company}' => $invoice->company,
					'{invoice_vat_number}' => $invoice->vat_number,
					'{invoice_firstname}' => $invoice->firstname,
					'{invoice_lastname}' => $invoice->lastname,
					'{invoice_address2}' => $invoice->address2,
					'{invoice_address1}' => $invoice->address1,
					'{invoice_city}' => $invoice->city,
					'{invoice_postal_code}' => $invoice->postcode,
					'{invoice_country}' => $invoice->country,
					'{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
					'{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
					'{invoice_other}' => $invoice->other,
					'{order_name}' => sprintf("#%06d", (int)($order->id)),
					'{date}' => Tools::displayDate(date('Y-m-d H:i:s'), (int)($order->id_lang), 1),
					'{carrier}' => $carrier->name,
					'{payment}' => Tools::substr($order->payment, 0, 32),
					'{products}' => $productsList,
					'{discounts}' => $discountsList,
					'{total_paid}' => Tools::displayPrice($order->total_paid, $currency, false),
					'{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $currency, false),
					'{total_discounts}' => Tools::displayPrice($order->total_discounts, $currency, false),
					'{total_shipping}' => Tools::displayPrice($order->total_shipping, $currency, false),
					'{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $currency, false));

					if (is_array($extraVars))
						$data = array_merge($data, $extraVars);

					// Join PDF invoice
/*					if ((int)(Configuration::get('PS_INVOICE')) AND Validate::isLoadedObject($orderStatus) AND $orderStatus->invoice AND $order->invoice_number)
					{
						$fileAttachment['content'] = PDF::invoice($order, 'S');
						$fileAttachment['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)($order->id_lang)).sprintf('%06d', $order->invoice_number).'.pdf';
						$fileAttachment['mime'] = 'application/pdf';
					}
					else
						$fileAttachment = NULL;*/

					/*if (Validate::isEmail($customer->email))
						Mail::Send((int)$order->id_lang, 'order_conf', Mail::l('Order confirmation', (int)$order->id_lang), $data, $customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, $fileAttachment);*/
				}
				$this->currentOrder = (int)$order->id;
				return true;
			}
			else
			{
				$errorMessage = Tools::displayError('Order creation failed');
				Logger::addLog($errorMessage, 4, '0000002', 'Cart', intval($order->id_cart));
				die($errorMessage);
			}
		}
		else
		{
			$errorMessage = Tools::displayError('Cart can\'t be loaded or an order has already been placed using this cart');
			Logger::addLog($errorMessage, 4, '0000001', 'Cart', intval($cart->id));
			die($errorMessage);
		}
	}

		/**
	 * @param Object Address $the_address that needs to be txt formated
	 * @return String the txt formated address block
	 */

	public function _getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
	{
		return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
	}
}