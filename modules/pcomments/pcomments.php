<?php
if (!defined('_PS_VERSION_'))
  exit;
 
class pcomments extends Module
{
	public function __construct()
	{
		$this->name = 'pcomments';
		$this->tab = 'front_office_features';
		$this->version = 1.0;
		$this->author = 'Polcoder';
		$this->need_instance = 0;
	 
		parent::__construct();
	 
		$this->displayName = $this->l('Polcoder Comments on the Homepage');
		$this->description = $this->l('Display product comments on the Homepage');
	}

	public function install()
	{
		if (parent::install() == false OR !$this->registerHook('displayHome'))
		return false;
		return true;
	}
	
	public function uninstall()
	{
	 	if (parent::uninstall() == false)
	 		return false;
	 	return true;
	}
	
	public function hookdisplayHome($params)    
	{        
		global $smarty;
		$count=0;		
		$get_comments = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_comment WHERE validate = 1 AND deleted = 0');		
		foreach ($get_comments as $comments)		
		{			
			$comment[$count]['title'] = $comments["title"];			
			$comment[$count]['content'] = $comments["content"];			
			$comment[$count]['customer'] = $comments["customer_name"];
			$customer_data = explode(" ",$comment[$count]['customer']);
			$comment[$count]['customer_name'] = $customer_data[0];
			$comment[$count]['customer_surname'] = substr($customer_data[1],0,1);
			$comment[$count]['grade'] = $comments["grade"];			
			$comment[$count]['date'] = $comments["date_add"];			
			$comment[$count]['product'] = $comments["id_product"];
			$comment[$count]['grade'] = $comments["grade"];	
			$id_product = $comments["id_product"];
			$get_image_id = Db::getInstance()->executeS('SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.$id_product.' AND cover = 1');	
			foreach ($get_image_id as $get_image)		
			{	
				$comment[$count]['image_id'] = $get_image["id_image"];	
			}		
			$count++;		
		}		
		$smarty->assign('comments', $comment);
		$smarty->assign('counter', $count-1);		
		$this->context->controller->addJS($this->_path.'fader.js');
		$this->context->controller->addCSS(($this->_path).'pcomments.css', 'all');        
		return $this->display(__FILE__ , 'pcomments.tpl');    
	}
}
?>