{if $counter > 0}
<div class="large-6 columns">
<div class="acc_head_3"><span>{l s='Product comments' mod='pcomments'}</span>{l s='Product comments' mod='pcomments'}<span>{l s='Product comments' mod='pcomments'}</span></div>
<div class="flexslider_comments carousel comment_box">
			<ul class="slides">
			{for $i=0 to $counter}
				<li>
					<div id="content">
						<div class="comment_left">
							<div class="comment_image"><a href="{$link->getProductLink($comments[$i]['product'])}"><img src="{$link->getImageLink($comments[$i]['product'], $comments[$i]['image_id'], 'small_default')}" /></a></div>
							<div class="comment_stars">
							{for $x=1 to 5}
								{if $x > $comments[$i]['grade']}
									<div class="star"></div>
								{else}
									<div class="star star_on"></div>
								{/if}
							{/for}
							</div>
						</div>
						<div class="comment_right">
							<div class="comment_title"><a href="{$link->getProductLink($comments[$i]['product'])}">{$comments[$i]['title']}</a></div>
							<div class="comment_content">{$comments[$i]['content']}</div>
							<div class="comment_customer">{$comments[$i]['customer_name']} {$comments[$i]['customer_surname']}</div>
							<div class="comment_date">{dateFormat date=$comments[$i]['date']|escape:'html':'UTF-8' full=0}</div>
						</div>
					</div>
					<div class="button_go">
						<a href="{$link->getProductLink($comments[$i]['product'])}" class="button_large">View</a>
					</div>
				</li> 
			{/for}
			</ul>
		</div>
</div>
{/if}