{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
		<meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" href="{$css_dir}foundation.css" />
        <link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>
        <link href="//fonts.googleapis.com/css?family=Arvo&subset=latin" rel="stylesheet" type="text/css">

{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<meta http-equiv="content-language" content="{$meta_language}" />
		<meta name="generator" content="PrestaShop" />
		<meta name="viewport" content="initial-scale=1">
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<script type="text/javascript">
			var baseDir = '{$content_dir}';
			var baseUri = '{$base_uri}';
			var static_token = '{$static_token}';
			var token = '{$token}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
		</script>

{if isset($js_files)}
	{foreach from=$js_files item=js_uri}
	
	<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
{/if}
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
	<link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
	{/foreach}
{/if}
		{$HOOK_HEADER}
<link rel="stylesheet" href="{$css_dir}ezmark.css" media="all">
<script type="text/javascript" language="Javascript" src="{$js_dir}jquery.ezmark.min.js"></script>
<script type="text/javascript" src="{$js_dir}scriptbreaker-multiple-accordion-1.js"></script>
	</head>
	
	<body {if isset($page_name)}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if} class="{if $hide_left_column}hide-left-column{/if} {if $hide_right_column}hide-right-column{/if}">
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
			<div id="page" class="large-12 columns">
				<div class="header_background"></div>
				<div class="row">
					<!-- Header -->
					<div id="header" class="small-12 columns">
					<div id="header_logo" class="small-12 columns">
							<a href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
                          
                            <!--<object data="logo.svg" width="302px" height="302px" type="image/svg+xml">
    <img src="onsale_en.gif" alt="PNG image of standAlone.svg" />-->
  </object>
								<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="300"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if} />
							</a>
						</div>
						<div id="header_right" class="small-12 columns">
							{$HOOK_TOP}
							
							{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}{include file="$tpl_dir./breadcrumb.tpl"}{/if}
							
							{if isset($category) && $page_name != 'product'}
								{if $category->id AND $category->active}
									<div class="PS_category_header">
										<h1>
											{strip}
												{$category->name|escape:'htmlall':'UTF-8'}
												{if isset($categoryNameComplement)}
													{$categoryNameComplement|escape:'htmlall':'UTF-8'}
												{/if}
											{/strip}
										</h1>
									<div>
								{/if}
							{/if}
							{if isset($manufacturer->id) && $page_name == 'manufacturer'}
									<div class="PS_category_header">
										<h1>
											{$manufacturer->name|escape:'htmlall':'UTF-8'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'new-products'}
									<div class="PS_category_header">
										<h1>
											{capture name=path}{l s='Novedades'}{/capture}
											{l s='Novedades'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'best-sales'}
									<div class="PS_category_header">
										<h1>
											{capture name=path}{l s='Top ventas'}{/capture}
											{l s='Top ventas'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'manufacturer' && !isset($manufacturer->id)}
									<div class="PS_category_header">
										<h1>
											{capture name=path}{l s='Nuestras marcas'}{/capture}
											{l s='Nuestras marcas'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'supplier'}
									<div class="PS_category_header">
										<h1>
											{capture name=path}{l s='Suppliers'}{/capture}
											{l s='Supplier: '}{$supplier->name|escape:'htmlall':'UTF-8'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'prices-drop'}
									<div class="PS_category_header">
										<h1>
											{capture name=path}{l s='Ofertas'}{/capture}
											{l s='Ofertas'}
										</h1>
									<div>
							{/if}
							{if $page_name == 'search'}
								<div class="PS_category_header">
									<h1>
										{l s='Búsqueda'}&nbsp;{if $nbProducts > 0}"{if isset($search_query) && $search_query}{$search_query|escape:'htmlall':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'htmlall':'UTF-8'}{elseif $ref}{$ref|escape:'htmlall':'UTF-8'}{/if}"{/if}
		{if isset($instantSearch) && $instantSearch}<a href="#" class="close">{l s='Volver a la página anterior'}</a>{/if}
									</h1>
								<div>
							{/if}
						</div>
					</div>
				</div>
				<div class="row" {if $page_name != 'index'}style="padding:0px"{/if}>
					<div id="columns" class="large-12 columns">
						
						<!-- Center -->
						{if $page_name != 'category' && $page_name != 'manufacturer' && $page_name != 'search' && $page_name != 'new-products' && $page_name != 'best-sales' && $page_name != 'prices-drop' && $page_name != 'supplier'}<div id="center_column_full">{/if}
						{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}<div id="center_column" style="margin-right: 10px;">{/if}
		{/if}