{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

			{if !$content_only}
					
<!-- Left -->
						{if $page_name == 'category' OR $page_name == 'manufacturer' OR $page_name == 'search' OR $page_name == 'new-products' OR $page_name == 'best-sales' OR $page_name == 'prices-drop' OR $page_name == 'supplier'}
						<div id="left_column">
							{$HOOK_LEFT_COLUMN}
						</div>
						{/if}
						
	<!-- Right -->{if $page_name == 'itsoff'}
					<div id="right_column" class="column grid_2 omega">
						{$HOOK_RIGHT_COLUMN}
					</div>
					{/if}
				</div>
			</div>
		</div><!-- END of Columns ROW -->
		<div class="row">
			<div class="large-12 columns">
				<div class="footer_space">
				<!-- Footer -->
					<div id="footer">
						{$HOOK_FOOTER}
                        
					</div>
                    
				</div>
			</div>
		</div>
	{/if}
	<script defer src="{$js_dir}slider/jquery.flexslider.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 240,
        itemMargin: 0,
      });
    });
    $(window).load(function(){
      $('.flexslider_comments').flexslider({
        animation: "fade",
        animationLoop: true,
        itemWidth: 600,
        itemMargin: 0,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
	</body>
</html>
