{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block languages module -->
{if count($languages) > 1}
<div class="languages_block">
	<div class="lang_header">
		{l s='language' mod='blocklanguages_mod'}
	</div>
	{foreach from=$languages key=k item=language name="languages"}
		{if $language.iso_code == $lang_iso}
			<div class="PC_selected_lang">{$language.iso_code}
		{/if}
	{/foreach}
	<div class="PC_selection_lang">
		<div class="PC_show_lang">
			{foreach from=$languages key=k item=language name="languages"}
				{if isset($lang_rewrite_urls.$indice_lang)}
					<span><a href="{$lang_rewrite_urls.$indice_lang|escape:htmlall}" title="{$language.name}">{$language.iso_code}</a></span>
				{else}
					<span><a href="{$link->getLanguageLink($language.id_lang)|escape:htmlall}" title="{$language.name}">{$language.iso_code}</a></span>
				{/if}
			{/foreach}
		</div>
	</div>
	</div>
</div>
{/if}
<!-- /Block languages module -->